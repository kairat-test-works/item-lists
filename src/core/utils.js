import React from 'react';
import {applySnapshot, onSnapshot} from 'mobx-state-tree';

import {clear, getItem, removeItem, setItem} from 'Internal';


export const Storage = {
  setItem, clear, getItem, removeItem,
};

export function genID() {
  return `_${Math.random().toString(36).substr(2, 9)}`;
}

export const pipeFn = (...fns) => (x) => fns.reduce((v, f) => f(v), x);

export const persist = (name, store, options, schema = {}) => {
  let hydrated = false;

  let storage = options.storage;

  if (typeof localStorage !== 'undefined' && localStorage === storage) {
    storage = Storage;
  }
  onSnapshot(store, _snapshot => {
    if (!hydrated) {
      return;
    }
    const snapshot = {..._snapshot};
    Object.keys(snapshot).forEach(key => {
      if (!schema[key]) {
        delete snapshot[key];
      }
    });
    const data = !options.jsonify ? snapshot : JSON.stringify(snapshot);
    storage.setItem(name, data);
  });

  storage.getItem(name).then(data => {
    if (data) {
      const snapshot = !options.jsonify ? data : JSON.parse(data);
      applySnapshot(store, snapshot);
      if (store.afterHydration && typeof store.afterHydration === 'function') {
        store.afterHydration();
      }
    }
    hydrated = true;
  });
};

export function withRef(Component) {
  class WithRefComponent extends React.Component {
    render() {
      const {forwardedRef, ...rest} = this.props;
      return (<Component ref={forwardedRef} {...rest} />);
    }
  }

  return React.forwardRef(
    (props, ref) => <WithRefComponent {...props} forwardedRef={ref}/>
  );
}
