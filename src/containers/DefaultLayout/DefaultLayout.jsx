import React from 'react';
import {Route, Switch, withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';

import Container from 'react-bootstrap/Container';

import {
  ITEMS_ROUTE,
  Items,
  DefaultHeader,
  pipeFn
} from 'Internal';


class DefaultLayout extends React.Component {

  render() {

    return (
      <React.Fragment>
        <DefaultHeader/>
        <Container className="py-5">
          <Switch>
            <Route exact name="Items" path={ITEMS_ROUTE} render={props => <Items {...props}/>}/>
          </Switch>
        </Container>
      </React.Fragment>
    );
  }
}

export default pipeFn(
  observer,
  inject('stores'),
  withRouter
)(DefaultLayout);
