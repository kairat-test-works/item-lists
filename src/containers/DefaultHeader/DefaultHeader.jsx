import React from 'react';
import {withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';

import {
  pipeFn
} from 'Internal';


class DefaultHeader extends React.Component {


  render() {

    return (
      <div>Header</div>
    );
  }
}

export default pipeFn(
  observer,
  inject('stores'),
  withRouter
)(DefaultHeader);

