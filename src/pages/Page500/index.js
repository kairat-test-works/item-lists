import React from 'react';
import loadable from '@loadable/component';

import {Loading} from 'Internal';


export const Page500 = loadable(
  () => import('./Page500'),
  {
    fallback: (<Loading/>),
  }
);
