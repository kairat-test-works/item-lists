import React from 'react';
import {withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';
import {createForm} from 'rc-form';

import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

import {
  pipeFn,
} from 'Internal';


class Signin extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      validated: false,
      loading: false,
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const {
      form,
      stores: {
        authStore,
      },
    } = this.props;

    form.validateFields((errs, vals) => {
      if (!errs) {
        this.setState({loading: true});

        authStore.signIn(vals).then(
          () => {
          },
          () => {
          },
        ).finally(
          () => this.setState({loading: false})
        );
      }

      this.setState({validated: true});
    });
  };

  render() {
    const {
      form: {
        getFieldDecorator,
        getFieldError,
      },
    } = this.props;

    const {
      validated,
      loading,
    } = this.state;

    return (
      <Container>
        <Form
          className="form-signin border rounded"
          onSubmit={this.handleSubmit}
          validated={validated}
          noValidate
        >
          <Form.Row>
            <Form.Group as={Col} controlId="username">
              <Form.Label>Username</Form.Label>
              {getFieldDecorator('username', {
                rules: [
                  {
                    required: true,
                    message: 'Required field',
                  },
                ],
              })(<Form.Control
                type="text"
                placeholder="Enter username"
                disabled={loading}
                required
              />)}
              {(getFieldError('username') || []).map((err, idx) => (
                <Form.Control.Feedback type="invalid" key={`username-err-${idx}`}>
                  {err}
                </Form.Control.Feedback>
              ))}
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group as={Col} controlId="password">
              <Form.Label>Password</Form.Label>
              {getFieldDecorator('password', {
                rules: [
                  {
                    required: true,
                    message: 'Required field',
                  },
                ],
              })(<Form.Control
                type="password"
                placeholder="Enter password"
                disabled={loading}
                required
              />)}
              {(getFieldError('password') || []).map((err, idx) => (
                <Form.Control.Feedback type="invalid" key={`password-err-${idx}`}>
                  {err}
                </Form.Control.Feedback>
              ))}
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Col>
              <Button
                className=""
                onClick={this.handleSubmit}
                disabled={loading}
                size="lg"
                block
              >
                {loading ? 'Signing in...' : 'Sign in'}
              </Button>
            </Col>
          </Form.Row>
        </Form>
      </Container>
    );
  }
}

export default pipeFn(
  observer,
  inject('stores'),
  withRouter,
  createForm({name: 'auth-form'})
)(Signin);
