import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'mobx-react';

import {
  RootStore,
  Storage,
  persist,
  AUTH_KEY,
} from 'Internal';

import './index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';


const rootStore = RootStore.create({
  authStore: {},
  itemStore: {},
  catStore: {},
}, {
  localStg: Storage,
});

persist(
  AUTH_KEY,
  rootStore.authStore,
  {
    storage: window.localStorage,
    jsonify: true
  },
  {
    isSigned: true,
  }
);

persist(
  AUTH_KEY,
  rootStore.catStore,
  {
    storage: window.localStorage,
    jsonify: true
  },
  {
    _list: true,
  }
);

persist(
  AUTH_KEY,
  rootStore.itemStore,
  {
    storage: window.localStorage,
    jsonify: true
  },
  {
    _list: true,
  }
);

ReactDOM.render(
  <React.StrictMode>
    <Provider stores={rootStore}>
      <App/>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
