import {types} from 'mobx-state-tree';

import {
  BaseModel,
} from 'Internal';


export const CategoryModel = BaseModel.named(
  'CategoryModel'
).props({
  id: types.identifier,
  name: types.maybeNull(types.string)
});


