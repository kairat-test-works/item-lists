import {types} from 'mobx-state-tree';

import {
  BaseModel,
} from 'Internal';


export const ItemModel = BaseModel.named(
  'ItemModel'
).props({
  id: types.identifier,
  name: types.string,
  price: types.optional(types.number, 0),
  duration: types.string,
  category_id: types.number
});


