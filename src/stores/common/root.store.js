import {types} from 'mobx-state-tree';

import {
  AuthStore,
  CategoryStore,
  ItemStore,
} from 'Internal';


export const RootStore = types.model('RootStore', {
  // domain
  authStore: types.late(() => AuthStore),
  itemStore: types.late(() => ItemStore),
  catStore: types.late(() => CategoryStore)
});
