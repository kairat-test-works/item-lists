import {getEnv, types} from 'mobx-state-tree';

import {
  AUTH_KEY,
  DomainStore,
} from 'Internal';


export const AuthStore = DomainStore.named(
  'AuthStore'
).props({
  isSigned: types.optional(types.boolean, false)
}).actions(self => {
  const {localStg} = getEnv(self);

  function signIn() {
    return new Promise((resolve) => {
      setTimeout(() => {
        self.runInAction(() => {
          self.isSigned = true;
          resolve();
        });
      }, 2000);
    });
  }

  function signOut() {
    return new Promise((resolve) => {
      self.runInAction(() => {
        self.isSigned = false;
        resolve();
      });
    });
  }

  function afterCreate() {
    localStg.getItem(AUTH_KEY).then(
      (val) => {
        if (!val) {
          localStg.setItem(AUTH_KEY, JSON.stringify({
            isSigned: false
          }));
        }
      },
      () => {
      },
    );
  }

  return {
    signIn,
    signOut,
    afterCreate,
  };
});
