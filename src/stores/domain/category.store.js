import {getEnv, getSnapshot, types} from 'mobx-state-tree';

import {
  DomainStore,
  CATS_KEY,
  CategoryModel,
} from 'Internal';


export const CategoryStore = DomainStore.named(
  'CategoryStore'
).props({
  _list: types.optional(
    types.array(types.late(() => CategoryModel)),
    []
  )
}).actions(self => {
  const {localStg} = getEnv(self);

  function get(_id) {
    return new Promise((resolve, reject) => {
      const found = self._list.find(cat => cat.id === _id);
      if (found) {
        resolve(found.snap);
      } else {
        reject('No category found');
      }
    });
  }

  function create(_data) {
    return new Promise((resolve) => {
      self.runInAction(() => {
        self._list.push(_data);
        resolve();
      });
    });
  }

  function update(_id, _data) {
    return new Promise((resolve, reject) => {
      self.runInAction(() => {
        const idx = self._list.findIndex(cat => cat.id === _id);
        if (idx > 0) {
          self._list.splice(idx, 1, _data);
          resolve();
        } else {
          reject('No category found');
        }
      });
    });
  }

  function remove(_id) {
    return new Promise((resolve) => {
      self.runInAction(() => {
        const idx = self._list.findIndex(cat => cat.id === _id);
        if (idx > 0) {
          self._list.splice(idx, 1);
        }
        resolve();
      });
    });
  }

  function afterCreate() {
    localStg.getItem(CATS_KEY).then(
      (val) => {
        if (!val) {
          localStg.setItem(CATS_KEY, JSON.stringify({
            _list: []
          }));
        }
      },
      () => {
      },
    );
  }

  return {
    get,
    create,
    update,
    remove,
    afterCreate,
  };
}).views(self => ({
  get list() {
    return getSnapshot(self._list);
  }
}));
