// This file is to control imports of project files
// to solve the issue - circular module dependencies
// read more details at https://medium.com/visual-development/how-to-fix-nasty-circular-dependency-issues-once-and-for-all-in-javascript-typescript-a04c987cf0de
//
// WARNING: order is IMPORTANT
//

// core
export * from 'Core/keys';
export * from 'Core/vars';
export * from 'Core/local-storage';
export * from 'Core/utils';

// models
export * from 'Stores/models/base.model';
export * from 'Stores/models/item.model';
export * from 'Stores/models/category.model';

// common stores
export * from 'Stores/common/base.store';
export * from 'Stores/common/ui.store';
export * from 'Stores/common/domain.store';

// domain stores
export * from 'Stores/domain/auth.store';
export * from 'Stores/domain/item.store';
export * from 'Stores/domain/category.store';

// root store
export * from 'Stores/common/root.store';

// components
export * from 'Components/index';

// containers
export * from 'Containers/index';

// views
export * from 'Views/index';

// pages
export * from 'Pages/index';

// routes
export * from './routes';
