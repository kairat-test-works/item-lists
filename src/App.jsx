import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import {
  DefaultLayout,
  Page404,
  PAGE404_ROUTE,
  Page500,
  PAGE500_ROUTE,
  ROOT_ROUTE,
  Signin,
  SIGNIN_ROUTE,
} from 'Internal';


function App() {

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path={PAGE404_ROUTE} name="Page 404" render={props => (<Page404 {...props}/>)}/>
        <Route exact path={PAGE500_ROUTE} name="Page 500" render={props => (<Page500 {...props}/>)}/>
        <Route exact path={SIGNIN_ROUTE} name="Sign in" render={props => (<Signin {...props}/>)}/>
        <Route path={ROOT_ROUTE} render={props => (<DefaultLayout {...props}/>)}/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
