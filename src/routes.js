export function passRouteParams(routeStr, params) {
  for (const [key, value] of Object.entries(params)) {
    routeStr = routeStr.replace(new RegExp(`:${key}`, 'i'), value);
  }
  return routeStr;
}

export const ROOT_ROUTE = '/';
export const PAGE404_ROUTE = '/404';
export const PAGE500_ROUTE = '/500';
export const SIGNIN_ROUTE = '/signin';
export const ITEMS_ROUTE = '/items';
export const CATEGORIES_ROUTE = '/categories';
