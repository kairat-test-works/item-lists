import React from 'react';
import loadable from '@loadable/component';

import {Loading} from 'Internal';


export const ItemForm = loadable(
  () => import('./ItemForm'),
  {
    fallback: (<Loading/>),
  }
);

export const ItemCreateModal = loadable(
  () => import('./modals/ItemCreateModal'),
  {
    fallback: (<Loading/>),
  }
);

export const ItemUpdateModal = loadable(
  () => import('./modals/ItemUpdateModal'),
  {
    fallback: (<Loading/>),
  }
);

export const Items = loadable(
  () => import('./Items'),
  {
    fallback: (<Loading/>),
  }
);
