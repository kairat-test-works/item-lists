import React from 'react';
import {withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';
import {createForm} from 'rc-form';

import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';

import {pipeFn, withRef} from 'Internal';


class ItemForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      validated: false,
    };
  }

  componentDidMount() {
    this.props.onRef(this);
  }

  componentWillUnmount() {
    this.props.onRef(undefined);
  }

  handleSubmit = () => {
    const {
      form,
      onSubmit,
    } = this.props;


    form.validateFields((errs, vals) => {
      if (!errs) {
        onSubmit(vals);
      }

      this.setState({validated: true});
    });
  }

  render() {
    const {
      stores: {
        catStore
      },
      form: {
        getFieldDecorator,
        getFieldError,
      },
      initials,
      loading,
    } = this.props;

    const {
      validated,
    } = this.state;

    getFieldDecorator('id', {
      initialValue: initials.id,
    });

    return (
      <Form noValidate>
        <Form.Row>
          <Form.Group as={Col}>
            <Form.Label>
              Name
            </Form.Label>
            {getFieldDecorator('name', {
              initialValue: initials.name,
              rules: [{
                required: true,
                message: 'Required field',
              }],
            })(<Form.Control
              type="text"
              disabled={loading}
              isInvalid={validated && getFieldError('name')}
              isValid={validated && !getFieldError('name')}
              required
            />)}
            {(getFieldError('name') || []).map((err, idx) => (
              <Form.Control.Feedback type="invalid" key={`name-err-${idx}`}>
                {err}
              </Form.Control.Feedback>
            ))}
          </Form.Group>
        </Form.Row>
        <Form.Row>
          <Form.Group as={Col}>
            <Form.Label>
              Price
            </Form.Label>
            {getFieldDecorator('price', {
              initialValue: initials.price,
              rules: [{
                type: 'number',
                required: true,
                message: 'Required field',
              }, {
                validator(_, val) {
                  return val > 0;
                },
                message: 'Incorrect value',
              }],
              normalize: val => (val ? parseFloat(val) : 0)
            })(<Form.Control
              type="number"
              min={0}
              disabled={loading}
              isInvalid={validated && getFieldError('price')}
              isValid={validated && !getFieldError('price')}
              required
            />)}
            {(getFieldError('price') || []).map((err, idx) => (
              <Form.Control.Feedback type="invalid" key={`number-err-${idx}`}>
                {err}
              </Form.Control.Feedback>
            ))}
          </Form.Group>
        </Form.Row>
        <Form.Row>
          <Form.Group as={Col}>
            <Form.Label>
              Duration
            </Form.Label>
            {getFieldDecorator('duration', {
              initialValue: initials.duration,
              rules: [{
                type: 'date',
                required: true,
                message: 'Required field',
              }]
            })(<Form.Control
              type="date"
              disabled={loading}
              isInvalid={validated && getFieldError('duration')}
              isValid={validated && !getFieldError('duration')}
              required
            />)}
            {(getFieldError('duration') || []).map((err, idx) => (
              <Form.Control.Feedback type="invalid" key={`duration-err-${idx}`}>
                {err}
              </Form.Control.Feedback>
            ))}
          </Form.Group>
        </Form.Row>
        <Form.Row>
          <Form.Group as={Col} controlId="category_id">
            <Form.Label>Category</Form.Label>
            {getFieldDecorator('category_id', {
              initialValue: initials.category_id,
              rules: [{
                type: 'number',
                required: true,
                message: 'Required field',
              }]
            })(<Form.Control
              as="select"
              disabled={loading}
              isInvalid={validated && getFieldError('category_id')}
              isValid={validated && !getFieldError('category_id')}
              required
            >
              {catStore.list.map((cat, idx) => (
                <option value={cat.id} key={`cat-${idx}`}>{cat.name}</option>
              ))}
            </Form.Control>)}
            {(getFieldError('category_id') || []).map((err, idx) => (
              <Form.Control.Feedback type="invalid" key={`category_id-err-${idx}`}>
                {err}
              </Form.Control.Feedback>
            ))}
          </Form.Group>
        </Form.Row>
      </Form>
    );
  }
}

export default pipeFn(
  observer,
  inject('stores'),
  withRouter,
  createForm({name: 'item-form'}),
  withRef,
)(ItemForm);
