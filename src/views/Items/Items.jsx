import React from 'react';
import {withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';

import {pipeFn} from 'Internal';

import {ItemCreateModal, ItemUpdateModal} from './index';


class Items extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      selected: {},
      cmShow: false,
      umShow: false,
    };
  }

  render() {
    const {
      stores: {
        itemStore
      },
    } = this.props;

    const {
      cmShow,
      umShow,
    } = this.state;

    return (
      <React.Fragment>
        <Row>
          <Col>
            <Button onClick={() => this.setState({cmShow: true})}>
              Add
            </Button>
          </Col>
        </Row>
        <Row>
          <Col>
            <Table className="my-3" striped bordered hover size="sm">
              <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Price</th>
                <th>Expires at</th>
                <th>Category</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
              {itemStore.list.map((item, idx) => (
                <tr key={`item-${idx}`}>
                  <td>{(idx + 1)}</td>
                  <td>{item.name}</td>
                  <td>{item.price}</td>
                  <td>{item.duration}</td>
                  <td>{item.category_id}</td>
                  <td>
                    <a href="#">Edit</a>{' '}
                    <a href="#">Delete</a>
                  </td>
                </tr>
              ))}
              {itemStore.list.length === 0 && (
                <tr>
                  <td colSpan={6} className="text-center">
                    No items
                  </td>
                </tr>
              )}
              </tbody>
            </Table>
          </Col>
        </Row>
        <ItemCreateModal
          show={cmShow}
          onHide={() => this.setState({cmShow: false})}
        />
        <ItemUpdateModal
          show={umShow}
          onHide={() => this.setState({umShow: false})}
        />
      </React.Fragment>
    );
  }
}

export default pipeFn(
  observer,
  inject('stores'),
  withRouter
)(Items);
