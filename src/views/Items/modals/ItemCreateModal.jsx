import React from 'react';
import {withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

import {
  pipeFn,
  genID,
} from 'Internal';

import {ItemForm} from '../index';


class ItemCreateModal extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false
    };
  }

  handleCreate = (vals) => {
    const {
      stores: {
        itemStore
      },
      onHide
    } = this.props;

    this.setState({loading: true});

    itemStore.create(vals).then(
      () => {
        onHide();
      },
      () => {
      },
    ).finally(
      () => this.setState({loading: false})
    );
  };

  render() {
    const {
      ...props
    } = this.props;

    return (
      <Modal {...props}>
        <Modal.Header closeButton>
          <Modal.Title>Create item</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ItemForm
            onRef={ref => this.form = ref}
            initials={{
              id: genID(),
              price: 0
            }}
            onSubmit={this.handleCreate}
            onCancel={props.onHide}
          />
        </Modal.Body>
        <Modal.Footer className="">
          <Button
            variant="secondary"
            onClick={props.onHide}
          >
            Close
          </Button>
          <Button
            variant="primary"
            onClick={() => {
              this.form.handleSubmit();
            }}
          >
            Save
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default pipeFn(
  observer,
  inject('stores'),
  withRouter
)(ItemCreateModal);
