import React from 'react';
import {withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';

import Modal from 'react-bootstrap/Modal';

import {
  pipeFn
} from 'Internal';

import {ItemForm} from '../index';
import Button from 'react-bootstrap/Button';


class ItemUpdateModal extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
    };
  }

  handleUpdate = ({id, ...vals}) => {
    const {
      stores: {
        itemStore
      },
      onHide
    } = this.props;

    this.setState({loading: true});

    itemStore.update(id, vals).then(
      () => {
        onHide();
      },
      () => {
      },
    ).finally(
      () => this.setState({loading: false})
    );
  };

  render() {
    const {
      vals,
      ...props
    } = this.props;

    const {
      loading
    } = this.state;

    return (
      <Modal {...props}>
        <Modal.Header closeButton>
          <Modal.Title>Edit item</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ItemForm
            onRef={ref => this.form = ref}
            initials={vals}
            onSubmit={this.handleUpdate}
            loading={loading}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => {
              this.form.handleSubmit();
            }}
            disabled={loading}
          >
            Close
          </Button>
          <Button
            variant="primary"
            onClick={props.onHide}
            disabled={loading}
          >
            Save
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default pipeFn(
  observer,
  inject('stores'),
  withRouter
)(ItemUpdateModal);
