/* craco.config.js */

const path = require('path');

module.exports = {
  plugins: [
    {
      plugin: {
        overrideWebpackConfig: ({webpackConfig, context: {paths}}) => {
          webpackConfig.resolve.alias = {
            ...webpackConfig.resolve.alias,
            Core: path.resolve(__dirname, `${paths.appSrc}/core/`),
            Assets: path.resolve(__dirname, `${paths.appSrc}/assets/`),
            Styles: path.resolve(__dirname, `${paths.appSrc}/styles/`),
            Stores: path.resolve(__dirname, `${paths.appSrc}/stores/`),
            Components: path.resolve(__dirname, `${paths.appSrc}/components/`),
            Containers: path.resolve(__dirname, `${paths.appSrc}/containers/`),
            Views: path.resolve(__dirname, `${paths.appSrc}/views/`),
            Pages: path.resolve(__dirname, `${paths.appSrc}/pages/`),
            Internal$: path.resolve(__dirname, `${paths.appSrc}/internal.js`),
          };

          return webpackConfig;
        }
      },
      options: {
        // preText: "Will log the webpack config:"
      }
    },
  ],
};
